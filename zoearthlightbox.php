<?php
/*
 * @package        plg_system_zoearthlightbox
*/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.plugin.plugin' );

$lang = JFactory::getLanguage();
$lang->load('plg_system_zoearthlightbox');

class  plgSystemZoearthLightBox extends JPlugin
{
    public function __construct(&$subject, $config)
    {
        parent::__construct($subject, $config);
    }

    function onAfterDispatch()
    {
        $document = JFactory::getDocument();
        $base = JPath::clean(JPATH_BASE);
        $site = JPath::clean(JPATH_SITE);
        if($base != $site)//20130617 zoearth 是否為後台
        {
            return false;
        }
        $administrator = JPath::clean(JPATH_ADMINISTRATOR);
        $target_div_id    = $this->params->get('target_div_id', '.itemBody');
        $img_width    = $this->params->get('img_width', 0);
        $img_height    = $this->params->get('img_height', 0);
        $document->addScript(JURI::root().'plugins/system/zoearthlightbox/assets/js/jquery.smooth-scroll.min.js');
        $document->addScript(JURI::root().'plugins/system/zoearthlightbox/assets/js/jquery-ui-1.8.18.custom.min.js');
        $document->addScript(JURI::root().'plugins/system/zoearthlightbox/assets/js/lightbox.js');
        $document->addScript(JURI::root().'plugins/system/zoearthlightbox/assets/js/add_script.js');
        $document->addStyleSheet(JURI::root().'plugins/system/zoearthlightbox/assets/css/lightbox.css');

        $document->addScriptDeclaration('
            var target_div_id = "'.$target_div_id.'";
            var img_width = "'.$img_width.'";
            var img_height = "'.$img_height.'";');
    }
}