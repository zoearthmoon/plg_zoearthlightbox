function get_imgs_to_lightbox(target_div_id,img_width,img_height)
{
    jQuery("img[class*="+target_div_id+"]").each(function(){
        jQuery(this).attr("rel","lightbox");
        jQuery(this).attr("href",jQuery(this).attr("src"));
        jQuery(this).addClass("cursor");
    });
    var lightbox, options;
    options = new LightboxOptions;
    return lightbox = new Lightbox(options);
}
jQuery(document).ready(function()
{
    get_imgs_to_lightbox(target_div_id,img_width,img_height);
});